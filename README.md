# kata-alpine-docker

Thanks to [Kata Containers](https://katacontainers.io), which runs containers isolated in Virtual Machines, 
it is possible to run docker within a container, without the need for dind. Privilege mode however is required.

See my blog [Container isolation with Kata and gVisor in Docker](https://marcelwiget.blog/2020/03/24/container-isolation-with-kata-and-gvisor-in-docker/)
for an initial overview of Kata (and gVisor).

Assuming kata-runtime is available, you can build your container with a running docker engine in it:

```
docker build -t kata-alpine-docker .
```

Then run the container 

```
$ docker run -ti -d --name kata-alpine-docker  --rm --privileged --runtime kata-runtime kata-alpine-docker
```

And log into the container with a shell and execute some docker commands:

```
$ docker exec -ti kata-alpine-docker sh

/ # docker ps
CONTAINER ID        IMAGE               COMMAND             CREATED             STATUS              PORTS               NAMES
/ # docker run -ti --rm ubuntu:20.04
Unable to find image 'ubuntu:20.04' locally
20.04: Pulling from library/ubuntu
d51af753c3d3: Pull complete
fc878cd0a91c: Pull complete
6154df8ff988: Pull complete
fee5db0ff82f: Pull complete
Digest: sha256:8bce67040cd0ae39e0beb55bcb976a824d9966d2ac8d2e4bf6119b45505cee64
Status: Downloaded newer image for ubuntu:20.04
root@31a8e9954519:/# uname -a
Linux 31a8e9954519 5.4.32-47.container #1 SMP Mon Apr 20 16:13:33 UTC 2020 x86_64 x86_64 x86_64 GNU/Linux
root@31a8e9954519:/# cat /etc/lsb-release
DISTRIB_ID=Ubuntu
DISTRIB_RELEASE=20.04
DISTRIB_CODENAME=focal
DISTRIB_DESCRIPTION="Ubuntu 20.04 LTS"
root@31a8e9954519:/# exit
exit
/ # docker ps
CONTAINER ID        IMAGE               COMMAND             CREATED             STATUS              PORTS               NAMES
/ # docker images
REPOSITORY          TAG                 IMAGE ID            CREATED             SIZE
ubuntu              20.04               1d622ef86b13        6 weeks ago         73.9MB
/ # docker network ls
NETWORK ID          NAME                DRIVER              SCOPE
5f8ce700b956        bridge              bridge              local
aa5c58f40d3f        host                host                local
3a9deb8112e7        none                null                local
/ # exit

$ docker ps
CONTAINER ID        IMAGE                COMMAND                  CREATED             STATUS              PORTS               NAMES
936c5ccc2b37        kata-alpine-docker   "/bin/sh -c /sbin/in…"   2 minutes ago       Up 2 minutes                            kata-alpine-docker
```

