all: build stop run

build:
	docker build -t kata-alpine-docker .

run:
	docker run -d --rm -ti --name kata-alpine-docker --runtime kata-runtime --privileged kata-alpine-docker

stop:
	-docker stop kata-alpine-docker
	-docker rm kata-alpine-docker
