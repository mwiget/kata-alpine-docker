FROM alpine:3.12
RUN apk add --no-cache docker openrc \
  && rc-update add docker default
ENTRYPOINT /sbin/init
